# webpack-tutorial-presentation

Main project:
https://github.com/lourencomcviana/webpack-tutorial
presentation
https://marp.app/

## Getting started
para todos after
- git checkout . 

### O básico
junta vários arquivos em um bundle e os apresenta
- git checkout 1.0
- npm install
- npx webpack --stats detailed
- apresentar pasta src
- abrir index.html no navegador

### Componentes
![image.png](./image.png)
- git checkout 2.0
- npm install
- npx webpack --stats detailed
- apresentar os loaders de css

### Sass
- git checkout 3.0
- npm install
- npx webpack --stats detailed
- mostrar loader do sass

### Plugins
- git checkout 4.0
- npm install
- npm run build
- mostrar plugins
- mostrar que página index.html não existe mais
- mostrar template handlebars (mostrar loader)
- mostrar nome dos bundles
- mostrar que os arquivos gerados tem hash em prod
- npm run build:dev
- mostrar que os arquivos gerados não tem mais hash

### Server
- git checkout 5.0
- npm install
- npm run build:dev
- npm run serve
- mostrar que agora temos configuração de dev e prod
- mostrar `output` no webpackconfig
- mostrar como loadash causa um split no shunk
* configuração npm run build não funciona nessa versão

### Projetos múltiplos
- git checkout 6.0
- npm install
- mostrar `entry` dentro do webpack.production.config.js
- mostrar os dois HtmlWebpackPlugin
- mostrar páginas com npm run serve

### 7.0-server-multi-page-app
- git checkout 7.0
- npm install
- npm run build
- npm run start
- mostrar server.js
- mostrar configuração do publicpath no arquivo do webpack
'
### 8.0 Module federation
- git checkout 8.0
- npm install em hello-world, dashboard, image-caption e muffin
- npm run start em todos
- mostrar module federation plugin nos webpacks
- enjoy
